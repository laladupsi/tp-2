from django.apps import AppConfig


class ProjectHubConfig(AppConfig):
    name = 'project_hub'
