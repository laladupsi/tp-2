from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [    
    path(r'hub/', index, name = 'hub'),
    url(r'projects/$', get_projects, name='get_projects'),
]

urlpatterns += staticfiles_urlpatterns()