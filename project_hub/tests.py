from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from news_article.models import Project
from .apps import ProjectHubConfig
from django.apps import apps
from news_article.models import Article

# Create your tests here.
class Project_hubTest(TestCase):

	def test_project_hub_using_index(self):
		found = resolve('/hub/')
		self.assertEqual(found.func, index)

	def test_project_hub_apps(self):
		self.assertEqual(ProjectHubConfig.name, 'project_hub')
		self.assertEqual(apps.get_app_config('project_hub').name, 'project_hub')

