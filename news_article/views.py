from django.shortcuts import render
from django.http import HttpResponseNotFound
from news_article.models import Article

# Create your views here.
def index(request):
	return render(request, 'article.html')

def generate_article_page(request, pk, slug):
    title = slug.replace("-", " ")    
    try:
        article = Article.objects.get(pk=pk, title=title)
        
    except Article.DoesNotExist:
        return HttpResponseNotFound("page not found")
    
    return render(request, 'article.html', {'article':article})