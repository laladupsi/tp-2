from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .apps import NewsArticleConfig
from django.apps import apps
# Create your tests here.
class newsArticle_test(TestCase):
	def test_url_is_active(self):
		response = Client().get('/article_page/')
		self.assertEqual(response.status_code, 200)

	def test_news_article_using_index(self):
		found = resolve('/article_page/')
		self.assertEqual(found.func, index)

	def test_news_hub_apps(self):
		self.assertEqual(NewsArticleConfig.name, 'news_article')
		self.assertEqual(apps.get_app_config('news_article').name, 'news_article')