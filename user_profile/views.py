from django.shortcuts import render
from project_item.models import Project
from project_item.models import Patreon
from django.http import JsonResponse
# Create your views here.
response = {}
def index(request):
    return render(request,'user_profile.html',response)

def get_json(request):
    nama = request.GET.get('name', None)
    result = []
    
    for donatedTo in Project.objects.all():
        for patreon in donatedTo.patreon.all():
            if(patreon.name == nama):
                result.append({
                    'program': donatedTo.title,
                    'amount': patreon.donation
                })

    return JsonResponse(result, safe=False)