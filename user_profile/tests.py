from django.test import TestCase
# Create your tests here.
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class UserProfile(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)
    
    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)
    
    def test_profile_using_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response,'user_profile.html')
