from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [    
    path('profile/', index, name = 'profile'),
    url(r'^get_json/',get_json,name='get_json'),
]

urlpatterns += staticfiles_urlpatterns()