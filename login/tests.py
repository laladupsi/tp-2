from django.test import TestCase
from django.test.client import Client

# Create your tests here.
class loginTest(TestCase):
    def test_login_url_is_exit(self):
        response = Client().get("/login/")
        self.assertEqual(response.status_code, 200)
        
    def test_logout_url_is_exit(self):
        response = Client().get("/logout/")
        self.assertEqual(response.status_code, 302)
