function showTesti() {
    $('tbody').empty();
    $.ajax({
        type: 'GET',
        url:'testimoni',
        data: {
            'name': {{user.username}},
            'description': $('#testi').val()
        }
        success: function(data) {
            var testi_cards = '<div class="maProjects row">';
            $.each(data, function(key, value){
                testi_cards += '<div class="col-sm-4 mb-4">';
                testi_cards += '<div class="card">';
                testi_cards += '<div class="card-body">';
                testi_cards += '<h5 class="card-title">'+ value.name +'</h5>';
                testi_cards += '<h6 class="card-subtitle mb-2 text-muted">'+ value.description +'</h6>';
                testi_cards += '</div></div></div>';
            });

            $(".maProjects").remove();
            $('#projects-container').append(testi_cards+="</div>");  

        }
    });
}