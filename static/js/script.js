function convertToSlug(Text){
    return Text
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
}