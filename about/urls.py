from django.urls import path
from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [    
    path(r'about/', index, name = 'about'),
    url(r'^testimoni/', add_testi, name= 'testimoni')
]

urlpatterns += staticfiles_urlpatterns()