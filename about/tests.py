from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .apps import AboutConfig
from django.apps import apps

from .models import Testimonials

# Create your tests here.
class About_test(TestCase):
	"""docstring for ClassName"""
	def test_url_is_active(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code, 200)

	def test_story8_using_index(self):
		found = resolve('/about/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_testi(self):
	# Creating a new testimoni
		new_testi = Testimonials.objects.create(description = 'test ppw')
		count_testi = Testimonials.objects.all().count()
		self.assertEqual(count_testi, 1)

	def test_model_exist(self):
		Testimonials.objects.create(name = "saryifa", description = "bismillah")
		count_testi = Testimonials.objects.all().count()
		self.assertEqual(count_testi, 1)

	def test_about_apps(self):
		self.assertEqual(AboutConfig.name, 'about')
		self.assertEqual(apps.get_app_config('about').name, 'about')

	def test_about_contain_title(self):
		title = 'About'
		response = Client().get('/about/')
		html_response = response.content.decode('utf8')
		self.assertIn(title, html_response)
