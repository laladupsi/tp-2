from django.contrib import admin

from .models import Testimonials

# Register your models here.
class TestimonialsModelAdmin(admin.ModelAdmin):
    list_display = ["name","pk"]    
    class Meta:
        model = Testimonials
        
admin.site.register(Testimonials, TestimonialsModelAdmin)