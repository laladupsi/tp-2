from django.shortcuts import render
from django.http import JsonResponse
from .models import Testimonials

# Create your views here.
def index(request):
	return render(request, 'about.html')

def add_testi(request):
    name = request.GET.get('name', None)
    desc = request.GET.get('description', None)
    validate_desc = not (len(desc) == 0 or desc.isspace())

    if request.user.is_authenticated and validate_desc:
        Testimonials.objects.create(name=name, description=desc)
    
    testimonials = Testimonials.objects.all()
    response = []
    for testi in testimonials:
        response.append({
            'name': testi.name,
            'description': testi.description
        })

    return JsonResponse(response, safe=False)