from django.test import TestCase
from django.test.client import Client
from django.test.client import resolve
from django.http import HttpRequest
from django.apps import apps
from .views import index, validate_donate, generate_donation_page
from project_item.models import Patreon

# Create your tests here.
class donateTest(TestCase):
    def test_donate_app_url_is_exit(self):
        response = Client().get("/donate/")
        self.assertEqual(response.status_code, 200)

    def test_landingpage_containt(self):
        response = self.client.get("/donate/")
        html_response = response.content.decode('utf8')
        self.assertIn('Donate Project', html_response)

    def test_can_create_object(self):
        Patreon.objects.create(name="admin", donation="10000")
        counting_all_available_status =  Patreon.objects.all().count();
        self.assertEqual(counting_all_available_status, 1)

    def test_donate_html_exist(self):
        response = Client().get('/donate/')
        self.assertTemplateUsed(response, 'donate.html')

    def test_using_function_index(self):
        found = resolve('/donate/')
        self.assertEqual(found.func, index)

    def test_validate_donate(self):
        response = Client().post("ajax/donate/", {'nomimal': '-10000', 'password':'admin', 'checkbox' : 'true', 'project_id' : '1'})
        self.assertEqual(response.status_code, 404)

    def test_generate_donate_page_if_logout(self):
        response = Client().get('/hub/project/1/BETIS-FASILKOM-UI-2019/donate/')

    def test_generate_donate_page(self):
        response = Client().get('/auth/login/google-oauth2/')
        Client().get('/hub/project/1/BETIS-FASILKOM-UI-2019/donate/')
        

