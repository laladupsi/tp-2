from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .apps import BaseConfig
from django.apps import apps

# Create your tests here.
class Base_test(TestCase):
	def test_url_is_active(self):
		response = Client().get('/index/')
		self.assertEqual(response.status_code, 200)

	def test_base_using_index(self):
		found = resolve('/index/')
		self.assertEqual(found.func, index)

	def test_base_apps(self):
		self.assertEqual(BaseConfig.name, 'base')
		self.assertEqual(apps.get_app_config('base').name, 'base')