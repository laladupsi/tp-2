from django.contrib import admin
from .models import Patreon, Project

# Register your models here.
class PatreonModelAdmin(admin.ModelAdmin):
    list_display = ["name", "donation","pk"]
    list_filter = ["donation"]
    
    class Meta:
        model = Patreon
admin.site.register(Patreon, PatreonModelAdmin)

class ProjectModelAdmin(admin.ModelAdmin):
    list_display = ["title", "category", "modified_date", "pk"]
    list_filter = ["category", "modified_date"]
    
    class Meta:
        model = Project
admin.site.register(Project, ProjectModelAdmin)