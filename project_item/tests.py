from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .apps import ProjectItemConfig
from django.apps import apps

# Create your tests here.
class Project_ItemTest(TestCase):

	def test_project_item_using_index(self):
		found = resolve('/projecthub/')
		self.assertEqual(found.func, index)

	def test_project_item_apps(self):
		self.assertEqual(ProjectItemConfig.name, 'project_item')
		self.assertEqual(apps.get_app_config('project_item').name, 'project_item')